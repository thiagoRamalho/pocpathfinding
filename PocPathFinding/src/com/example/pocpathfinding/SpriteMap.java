package com.example.pocpathfinding;

import org.andengine.entity.sprite.Sprite;
import org.andengine.util.algorithm.path.IPathFinderMap;

import android.util.Log;

public class SpriteMap implements IPathFinderMap<Sprite>{

	private Sprite[][] map;
	private final int maxX;
	private final int maxY;
	
	public SpriteMap(int maxX, int maxY) {
		this.maxX = 1+maxX;
		this.maxY = 1+maxY;
		this.map = new Sprite[this.maxX][this.maxY];
		this.iniciar();
	}
	
	public void iniciar(){
		for (int x = 0; x < maxX; x++) {

			for (int y = 0; y < maxY; y++) {
				
				this.map[x][y] = null;
			}
		}
	}
	
	@Override
	public boolean isBlocked(int pX, int pY, Sprite pEntity) {
		
		Sprite sprite = map[pX][pY];

		boolean bloqueado = sprite != null;

		Log.d("isBlocked", " bloqueado: "+(pX)+" "+(pY)+" "+(bloqueado));

		return bloqueado;
	}
	
	public void put(Sprite value){
		
		value.setOffsetCenter(0, 0);

		
		int row    = (int) (value.getX()/value.getWidth());
		int column = (int) (value.getY()/value.getHeight());
		
		String key = row+" | "+column;
		
		Log.d("mapa", key);
		
		map[row][column] = value;
		
		value.setX(row * value.getWidth());
		value.setY(column * value.getHeight());
		
	}
}
