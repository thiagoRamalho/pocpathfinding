package com.example.pocpathfinding;

import java.io.IOException;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXProperties;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.extension.tmx.TMXTileProperty;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.adt.color.Color;
import org.andengine.util.algorithm.path.Path;
import org.andengine.util.algorithm.path.astar.AStarPathFinder;
import org.andengine.util.algorithm.path.astar.ManhattanHeuristic;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

/**
 * PathFinding
 * @author thiago
 *
 */
public class MainActivity extends SimpleBaseGameActivity implements IOnSceneTouchListener{

	private float CAMERA_WIDTH = 0;
	private float CAMERA_HEIGHT = 0;

	private ITexture mFaceTexture;
	private ITextureRegion mFaceTextureRegion;
	private Camera camera;
	private CustomSprite player;
	private PathModifier pathModifier;
	private CustomTiledMap map;

	@Override
	public EngineOptions onCreateEngineOptions() {

		final DisplayMetrics displayMetrics = new DisplayMetrics();
		this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		CAMERA_WIDTH = displayMetrics.widthPixels;
		CAMERA_HEIGHT= displayMetrics.heightPixels;

		camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);

		return new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), camera);
	}

	@Override
	public void onCreateResources() throws IOException {
		this.mFaceTexture = new AssetBitmapTexture(this.getTextureManager(), this.getAssets(), "gfx/face_box.png");
		this.mFaceTextureRegion = TextureRegionFactory.extractFromTexture(this.mFaceTexture);
		this.mFaceTexture.load();
	}

	@Override
	public Scene onCreateScene() {
		return tmxTiled();
	}
	
	
	private Scene tmxTiled(){

		VertexBufferObjectManager vbom = this.getVertexBufferObjectManager();
		
		final Scene scene = new Scene();
		scene.setOffsetCenter(0, 0);

		try {

			player = new CustomSprite(32, 32, this.mFaceTextureRegion, vbom, null);

			player.setColor(Color.RED);
			player.setOffsetCenter(0, 0);
	
			scene.getBackground().setColor(0.09804f, 0.6274f, 0.8784f);

			this.mEngine.registerUpdateHandler(new FPSLogger());

			TMXLoader tmxLoader = new TMXLoader(this.getAssets(), getTextureManager(), vbom);

			TMXTiledMap tmxtiledMap = tmxLoader.loadFromAsset("tmx/cenario3.tmx");
			
			tmxtiledMap.setOffsetCenter(0, 0);

			map = new CustomTiledMap(tmxtiledMap.getTileColumns(), tmxtiledMap.getTileRows());

			TMXLayer tmxLayer = tmxtiledMap.getTMXLayers().get(0);

			TMXTile[][] tmxTiles = tmxLayer.getTMXTiles();
			
			for (TMXTile[] tmxTile : tmxTiles) {

				for (TMXTile tmxTile2 : tmxTile) {
					
					float pX = 32 * tmxTile2.getTileColumn();
					float pY = 32 * tmxTile2.getTileRow();
					ITextureRegion textureRegion = tmxTile2.getTextureRegion();
					TMXProperties<TMXTileProperty> tmxTileProperties = tmxTile2.getTMXTileProperties(tmxtiledMap);
					
					Log.d("x|y", ""+pX +"| "+pY);
					
					CustomSprite sprite = new CustomSprite(pX, pY, textureRegion, vbom, tmxTileProperties);
					map.put(sprite);
					scene.attachChild(sprite);
				}
			}
			scene.attachChild(player);

		} catch (TMXLoadException e) {
			Log.e("TMXTILED", "ERROR", e);
		}

		this.toastOnUiThread("WIDTH: " + this.CAMERA_WIDTH,   Toast.LENGTH_SHORT);
		this.toastOnUiThread("HEIGHT: " + this.CAMERA_HEIGHT, Toast.LENGTH_SHORT);

		scene.setOnSceneTouchListener(this);
		
		return scene;
	}
	
	
	private void createPath(CustomSprite player, int toX, int toY, CustomTiledMap map){
		
		int coordX = (int) (CAMERA_WIDTH / player.getWidth());
		int coordY = (int) (CAMERA_HEIGHT / player.getHeight());
		
		int fromX = 0;
		int fromY = 0;
		
		this.toastOnUiThread("from: "+ fromX + " | " + fromY,   Toast.LENGTH_SHORT);
		
		if(toX == (int)player.getX() && toY == (int)player.getY()){
			return;
		}
		
		AStarPathFinder<CustomSprite> aStarPathFinder = new AStarPathFinder<CustomSprite>();

		Path findPath = aStarPathFinder.findPath(map, fromX, fromY, coordX, coordY, 
				player, (int) ((int)player.getX()/player.getWidth()), (int) ((int)player.getY()/player.getHeight()), toX, toY, 
				false, 
				new ManhattanHeuristic<CustomSprite>(), new CustoTiled());

		if(findPath == null){
			return;
		}
		
		org.andengine.entity.modifier.PathModifier.Path p = new org.andengine.entity.modifier.PathModifier.Path(findPath.getLength());

		for(int i=0;i < findPath.getLength();i++)
		{
			float x =  (findPath.getX(i) * player.getWidth());
			float y =  (findPath.getY(i) * player.getHeight());
			p.to(x,y);
			Log.d("caminho", "x ="+ x + " y= "+y);
		}
		
		pathModifier = new PathModifier(5, p);
		pathModifier.setAutoUnregisterWhenFinished(true);
		player.registerEntityModifier(pathModifier);
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {

		Log.d("touch", ""+pSceneTouchEvent.getAction());
		
		if(pSceneTouchEvent.getAction() == MotionEvent.ACTION_DOWN && (pathModifier == null || pathModifier.isFinished())){
			
			int pX = (int) pSceneTouchEvent.getMotionEvent().getX();
            int pY = (int) pSceneTouchEvent.getMotionEvent().getY();
            
            pY = (int) (CAMERA_HEIGHT - pY);
            
			pX = (int) (pX/player.getWidth());
			pY = (int) (pY/player.getHeight());
			
			this.toastOnUiThread("touch: "+ pX + " | " + pY,   Toast.LENGTH_SHORT);
            this.createPath(player, pX, pY, map);
		}
		
		return true;
	}

}

