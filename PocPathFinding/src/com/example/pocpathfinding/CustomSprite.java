package com.example.pocpathfinding;

import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.tmx.TMXProperties;
import org.andengine.extension.tmx.TMXTileProperty;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.util.Log;

public class CustomSprite extends Sprite{
	
	private TMXProperties<TMXTileProperty> propriedades;

	public CustomSprite(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager, TMXProperties<TMXTileProperty> tmxTileProperties) {

		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.propriedades = tmxTileProperties == null ? new TMXProperties<TMXTileProperty>() : tmxTileProperties;
		this.setOffsetCenter(0, 0);
		
		Log.d("tile | constructor", this.toString());
	}

	
	
	
	@Override
	public String toString() {
		return "CustomSprite [propriedades=" + propriedades + ", mX=" + mX
				+ ", mY=" + mY + "]";
	}

	public Object getValue(String key){
		
		Object value = null;
		
		if(this.propriedades.contains(key)){
			
			int index = this.propriedades.lastIndexOf(key);
			value = this.propriedades.get(index);
		}
		
		return value;
	}

	public boolean isBloqued() {
		return this.propriedades.containsTMXProperty("bloqueado","true");
	}
}
