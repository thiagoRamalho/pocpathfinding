package com.example.pocpathfinding;

import org.andengine.util.algorithm.path.IPathFinderMap;

import android.util.Log;

public class CustomTiledMap implements IPathFinderMap<CustomSprite>{

	private CustomSprite[][] map;
	private final int maxX;
	private final int maxY;
	
	public CustomTiledMap(int maxX, int maxY) {
		this.maxX = 1+maxX;
		this.maxY = 1+maxY;
		this.map = new CustomSprite[this.maxX][this.maxY];
		this.iniciar();
	}
	
	public void iniciar(){
		for (int x = 0; x < maxX; x++) {

			for (int y = 0; y < maxY; y++) {
				
				this.map[x][y] = null;
			}
		}
	}
	
	@Override
	public boolean isBlocked(int pX, int pY, CustomSprite pEntity) {

		boolean isBlocked = false;
		CustomSprite tile = this.map[pX][pY];
		
		if(tile != null){
			isBlocked = tile.isBloqued();

			Log.d("tile | isBlocked", tile.toString());
		}

		Log.d("isBloqued | isBlocked", isBlocked+"");
		
		return isBlocked;
	}
	
	public void put(CustomSprite value){
		
		value.setOffsetCenter(0, 0);
		
		int row    = (int) (value.getX()/value.getWidth());
		int column = (int) (value.getY()/value.getHeight());
		
		map[row][column] = value;
	}
}
